import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroupDirective} from '@angular/forms';
import {Router} from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  myGroup;
  userId;
  constructor(private http: HttpClient, private formBuilder: FormBuilder, private router: Router
  ) {
    this.userId = 5;
    this.myGroup = this.formBuilder.group({
      firstName: '',
      password: ''
    });
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  onSubmit(loginForm: FormGroupDirective) {
    const userName = loginForm.value.firstName;
    const pass = loginForm.value.password;
    this.http.post<any>('https://jsonplaceholder.typicode.com/posts', { title: 'Angular POST Request Example' }).subscribe(data => {
        this.userId = data.id;
    });
    console.log(this.userId);
    if (pass === '123') {
      this.router.navigate(['/home']);
    }
    console.log(loginForm.value);
  }
}
