import {Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  hero = {
    id: 1,
    name: ''
  };

  constructor() {
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  search() {
    alert(this.hero.name);
  }
}
